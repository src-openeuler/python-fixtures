Name:           python-fixtures
Version:        4.1.0
Release:        1
Summary:        A python contract for writing reusable state / support logic tests
License:        ASL 2.0 or BSD
URL:            https://launchpad.net/python-fixtures
Source0:        https://pypi.python.org/packages/source/f/fixtures/fixtures-%{version}.tar.gz
BuildArch:      noarch

%description
Fixtures is a python contract that provides reusable state / support logic
for unit testing. It includes some helper and adaptation logic to write your
own fixtures using the fixtures contract.

%package -n python3-fixtures
Summary:        A python3 contract for reusable state / support logic
BuildArch:      noarch
BuildRequires:  python3-devel python3-pbr >= 5.7.0 python3-mock python3-testtools >= 2.5.0
Requires:       python-extras python-pbr >= 5.7.0 python3-testtools >= 2.5.0
%{?python_provide:%python_provide python3-fixtures}

%description -n python3-fixtures
Fixtures is a python3 contract that provides reusable state / support logic
for unit testing. It includes some helper and adaptation logic to write your
own fixtures using the fixtures contract.

%prep
%autosetup -n fixtures-%{version} -p1

%build
export PBR_VERSION=%{version}
%py3_build

%install
export PBR_VERSION=%{version}
%py3_install

%check
%{__python3} -m testtools.run fixtures.test_suite

%files -n python3-fixtures
%doc README.rst GOALS NEWS Apache-2.0 BSD COPYING
%{python3_sitelib}/fixtures
%{python3_sitelib}/fixtures-%{version}-py%{python3_version}.egg-info

%changelog
* Thu Jan 11 2024 liyanan <liyanan61@h-partners.com> - 4.1.0-1
- Update to 4.1.0

* Thu Jul 7 2022 baizhonggui <baizhonggui@h-partners.com> - 4.0.1-1
- Update to 4.0.1

* Fri Apr 1 2022 caodongxia <caodongxia@huawei.com> - 3.0.0-15
- Fix test_monkeypatch failed due to python3.10

* Tue Sep 29 2020 liuweibo <liuweibo10@huawei.com> - 3.0.0-14
- Fix Source0

* Mon Aug 10 2020 lingsheng <lingsheng@huawei.com> - 3.0.0-13
- Remove python2-fixtures subpackage

* Mon Nov 25 2019 Ling Yang <lingyang2@huawei.com> - 3.0.0-12
- Package init
